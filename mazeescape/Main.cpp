#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);

    
    //
    //GAME SETUP
    //
    
    //game clock
    //Create a clock to track time passed between frames
    sf::Clock gameClock;
    
    //seed random number generator
    srand(time(NULL));

    // Game Music
    sf::Music gameMusic;
    // load our audio using a file path
    gameMusic.openFromFile("Assets/Audio/music.ogg");//alter path
    // start the music
    gameMusic.play();

    // Game Font
    sf::Font gameFont;
    // load our font using a file path
    gameFont.loadFromFile("Assets/Font/mainFont.ttf");//alter path
    
    //Set game over variable
    bool gameOver = false;

    // Game Over Text
    // Declare a text variable called gameOverText to hold our game over display
    sf::Text gameOverText;
    // Set the font our text should use
    gameOverText.setFont(gameFont);
    // Set the string of text that will be displayed by this text object
    gameOverText.setString("GAME OVER\n\npress R to restart\nor Q to quit");
    // Set the size of our text, in pixels
    gameOverText.setCharacterSize(72);
    // Set the colour of our text
    gameOverText.setFillColor(sf::Color::Cyan);
    // Set the text style for the text
    gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
    // Position our text in the top center of the screen
    gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

    //Game Loop
    
	while (gameWindow.isOpen())
	{
        //
        //INPUT SECTION
        //
        
		sf::Event gameEvent;
		while (gameWindow.pollEvent(event))
		{
			if (gameEvent.type == sf::Event::Closed)
			{
				gameWindow.close();
			}


		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			gameWindow.close();
		}

		if (gameOver || winState) 
		{

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				gameOver = false;
				winState = false;
				playerObject.Reset();
				score = 0;
				gameMusic.play();
				enemies.clear();
				spawnIndex = 0;
			}
		}
        //player keybind input
        playerObject.Input();
        
        //
        //UPDATE SECTION
        //
        
        //get time passed since last frame and reset game clock
        sf::Time frameTime = gameClock.restart();
        
        //update player
        playerObject.Update(frameTime);
        
        //
        //DRAW SECTION
        //
        
        //clear section to single colour
        gameWindow.clear(sf::Color(15,15,15));
        
        //display window contents
        gameWindow.display();
	}

	return 0;
}